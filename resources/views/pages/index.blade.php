@extends('layout.main')

@section('search')
 <section>
            <div class="container">
                <div class="search-box-wrap-layout2">
                    <div class="search-box-layout1">
                        <form action="{{ url('search') }}" method="post">
                                                        {{ csrf_field() }}

                            <div class="row no-gutters">
                                <div class="col-lg-4 form-group">
                                    <div class="input-search-btn search-location">
                                        <i class="fas fa-map-marker-alt"></i>
                                        <input type="text" class="form-control" placeholder="Select Location" name="location">
                                    </div>
                                </div>
                                
                                <div class="col-lg-6 form-group">
                                    <div class="input-search-btn search-keyword">
                                        <i class="fas fa-text-width"></i>
                                        <input type="text" class="form-control" placeholder="Enter Keyword here ..." value="{{old('keyword')}}" name="keyword">
                                    </div>
                                </div>
                                <div class="col-lg-2 form-group">
                                    <button type="submit" class="submit-btn"><i class="fas fa-search"></i>Search</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
@endsection
@section('contents')

<div class="col-xl-12 col-lg-12">
                        <div class="single-product-box-layout1">
                            <div class="item-related-product light-shadow-bg">
                                <div class="flex-heading-layout2">
                                    <h3 class="widget-border-title">Ads</h3>
                                    <div id="owl-nav1" class="smart-nav-layout1">
                                        <span class="rt-prev">
                                            <i class="fas fa-angle-left"></i>
                                        </span>
                                        <span class="rt-next">
                                            <i class="fas fa-angle-right"></i>
                                        </span>
                                    </div>
                                </div>
                                <div class="light-box-content">
                                    <div class="rc-carousel" data-loop="true" data-items="4" data-margin="30" data-custom-nav="#owl-nav1" data-autoplay="false" data-autoplay-timeout="3000" data-smart-speed="1000" data-dots="false" data-nav="false" data-nav-speed="false" data-r-x-small="1" data-r-x-small-nav="false" data-r-x-small-dots="false" data-r-x-medium="2" data-r-x-medium-nav="false" data-r-x-medium-dots="false" data-r-small="2" data-r-small-nav="false" data-r-small-dots="false" data-r-medium="2" data-r-medium-nav="false" data-r-medium-dots="false" data-r-large="3" data-r-large-nav="false" data-r-large-dots="false" data-r-extra-large="3" data-r-extra-large-nav="false" data-r-extra-large-dots="false">
                                        <div class="product-box-layout1 box-shadwo-light mg-1">
                                            <div class="item-img">
                                                <a href="single-product1.html"><img src="media/product/product1.jpg" alt="Product"></a>
                                            </div>
                                            <div class="item-content">
                                                <h3 class="item-title"><a href="single-product1.html">HD 27 inch Mac 1 year used only</a><span>New</span></h3>
                                                <ul class="entry-meta">
                                                    <li><i class="far fa-clock"></i>4 months ago</li>
                                                    <li><i class="fas fa-map-marker-alt"></i>Port Chester, New York</li>
                                                </ul>
                                                <div class="item-price">
                                                    <span class="currency-symbol">$</span>
                                                    2,500
                                                </div>
                                            </div>
                                        </div>
                                        <div class="product-box-layout1 box-shadwo-light mg-1">
                                            <div class="item-img">
                                                <a href="single-product1.html"><img src="media/product/product2.jpg" alt="Product"></a>
                                            </div>
                                            <div class="item-content">
                                                <h3 class="item-title"><a href="single-product1.html">New Banded Smart Watch</a><span>New</span></h3>
                                                <ul class="entry-meta">
                                                    <li><i class="far fa-clock"></i>4 months ago</li>
                                                    <li><i class="fas fa-map-marker-alt"></i>Kansas, Emporia</li>
                                                </ul>
                                                <div class="item-price">
                                                    <span class="currency-symbol">$</span>
                                                    47
                                                </div>
                                            </div>
                                        </div>
                                        <div class="product-box-layout1 box-shadwo-light mg-1">
                                            <div class="item-img">
                                                <a href="single-product1.html" class="item-trending"><img src="media/product/product3.jpg" alt="Product"></a>
                                            </div>
                                            <div class="item-content">
                                                <h3 class="item-title"><a href="single-product1.html">High quality brand new headphone</a><span>New</span></h3>
                                                <ul class="entry-meta">
                                                    <li><i class="far fa-clock"></i>4 months ago</li>
                                                    <li><i class="fas fa-map-marker-alt"></i>New York, Mineola</li>
                                                </ul>
                                                <div class="item-price">
                                                    <span class="currency-symbol">$</span>
                                                    15
                                                </div>
                                            </div>
                                        </div>
                                        <div class="product-box-layout1 box-shadwo-light mg-1">
                                            <div class="item-img">
                                                <a href="single-product1.html"><img src="media/product/product4.jpg" alt="Product"></a>
                                            </div>
                                            <div class="item-content">
                                                <h3 class="item-title"><a href="single-product1.html">Running shoes imported from china</a><span>New</span></h3>
                                                <ul class="entry-meta">
                                                    <li><i class="far fa-clock"></i>4 months ago</li>
                                                    <li><i class="fas fa-map-marker-alt"></i>California, Bakersfield</li>
                                                </ul>
                                                <div class="item-price">
                                                    <span class="currency-symbol">$</span>
                                                    36
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
<div class="col-xl-12 col-lg-12">
                        <div class="heading-layout2">
                            <h2 class="heading-title">Recommend</h2>
                        </div>
                        <div class="row">
                            @if(!empty($Ads))
                            @foreach($Ads as $key)
                            <div class=" col-md-3">
                                <div class="product-box-layout1">
                                    <div class="item-img">
                                        <a href="" class="item-trending"><img style="
                                                        max-width: 200px;
                                                        max-height: 175px;
                                                        /* margin: -37px; */
                                                        margin-left: 50px;
                                                " src="{{$key['images']['image']}}" alt="Product">
                                        </a>
                                    </div>
                                    <div class="item-content">
                                        <div class="item-tag"><a href="#">{{$key['category']['name']}}</a></div>
                                        <h3 class="item-title"><a href="ad-details/{{$key['id']}}">{{$key['title']}}</a></h3>
                                        <ul class="entry-meta">
                                            <li><i class="far fa-clock"></i>{{$key['created_at']}}</li>
                                            <li><i class="fas fa-map-marker-alt"></i>{{$key['city']}}, {{$key['state']}} </li>
                                        </ul>
                                        <div class="item-price">
                                            <span class="currency-symbol">₹</span>
                                            {{$key['price']}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                            @else
                            <h1>Data Not Found</h1>
                            @endif
                    
                        </div>
                    </div>


                    
@endsection


