@include('layout.header')
<section class="inner-page-banner" data-bg-image="{{ url('media/banner/banner1.jpg') }}">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        
                    </div>
                </div>
            </div>
        </section>
        <!--=====================================-->
        <!--=          Product Start         =-->
        <!--=====================================-->
        <section class="single-product-wrap-layout1 section-padding-equal-70 bg-accent">
            <div class="container">
                <div class="row">
                    <div class="col-xl-9 col-lg-8">
                        <div class="single-product-box-layout1">
                            <div class="product-info light-shadow-bg">
                                <div class="product-content light-box-content">
                                    <div class="item-img-gallery">
                                        <div class="tab-content">
                                        	@foreach($Ads as $key)
                                        	{{-- @foreach($key['images'] as $image) --}}
                                        	
                                            <div class="tab-pane fade show active" id="gallery1" role="tabpanel">
                                                <a href="#">
                                                    <img class="zoom_01" src="{{ url($key['images']['image']) }}" alt="product" data-zoom-image="media/product/product24.jpg">
                                                </a>
                                            </div>
                                            {{-- @endforeach                                             --}}
                                            @endforeach
                                            @foreach($Ads as $key)
                                        	@foreach($key['multipleimages'] as $images)
                                        	{{-- @dd($key['multipleimages']) --}}
                                        	
                                            <div class="tab-pane fade show" id="gallery{{$loop->iteration}}" role="tabpanel">
                                                <a href="#">
                                                    <img class="zoom_01" src="{{ url($images['image']) }}" alt="product" data-zoom-image="media/product/product24.jpg">
                                                </a>
                                            </div>
                                            @endforeach
                                            @endforeach
                                         {{--    <div class="tab-pane fade" id="gallery2" role="tabpanel">
                                                <a href="#">
                                                    <img class="zoom_01" src="media/product/product25.jpg" alt="product" data-zoom-image="media/product/product25.jpg">
                                                </a>
                                            </div>
                                            <div class="tab-pane fade" id="gallery3" role="tabpanel">
                                                <a href="#">
                                                    <img class="zoom_01" src="media/product/product26.jpg" alt="product" data-zoom-image="media/product/product26.jpg">
                                                </a>
                                            </div>
                                            <div class="tab-pane fade" id="gallery4" role="tabpanel">
                                                <a href="#">
                                                    <img class="zoom_01" src="media/product/product27.jpg" alt="product" data-zoom-image="media/product/product27.jpg">
                                                </a>
                                            </div>
                                            <div class="tab-pane fade" id="gallery5" role="tabpanel">
                                                <a href="#">
                                                    <img class="zoom_01" src="media/product/product28.jpg" alt="product" data-zoom-image="media/product/product28.jpg">
                                                </a>
                                            </div> --}}
                                        </div>
                                        <ul class="nav nav-tabs" role="tablist">
                                        	@foreach($Ads as $key)
                                        	@foreach($key['multipleimages'] as $images)
                                            <li class="nav-item">
                                                <a class="nav-link active" data-toggle="tab" href="#gallery{{$loop->iteration}}" role="tab" aria-selected="true">
                                                    <img src="{{ url($images['image']) }}" alt="thumbnail">
                                                </a>
                                            </li>
                                            @endforeach
                                            @endforeach
                                         {{--    <li class="nav-item">
                                                <a class="nav-link" data-toggle="tab" href="#gallery2" role="tab" aria-selected="false">
                                                    <img src="media/product/product30.jpg" alt="thumbnail">
                                                </a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" data-toggle="tab" href="#gallery3" role="tab" aria-selected="false">
                                                    <img src="media/product/product31.jpg" alt="thumbnail">
                                                </a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" data-toggle="tab" href="#gallery4" role="tab" aria-selected="false">
                                                    <img src="media/product/product32.jpg" alt="thumbnail">
                                                </a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" data-toggle="tab" href="#gallery5" role="tab" aria-selected="false">
                                                    <img src="media/product/product33.jpg" alt="thumbnail">
                                                </a>
                                            </li> --}}
                                        </ul>
                                    </div>


                                    <div class="single-entry-meta">
                                        <ul>
                                            <li><i class="far fa-clock"></i>{{$Ads[0]['created_at']}}</li>
                                            <li><i class="fas fa-map-marker-alt"></i>{{$key['city']}}, {{$key['state']}}</li>
                                            {{-- <li><i class="far fa-eye"></i>223 views</li> --}}
                                        </ul>
                                        {{-- <div class="item-condition">New</div> --}}
                                    </div>
                                    <div class="item-details">
                                        <ul class="nav nav-tabs" role="tablist">
                                            <li class="nav-item">
                                                <a class="nav-link active" data-toggle="tab" href="#details" role="tab" aria-selected="true">Description</a>
                                            </li>
                                            {{-- <li class="nav-item">
                                                <a class="nav-link" data-toggle="tab" href="#features" role="tab" aria-selected="false">Features</a>
                                            </li> --}}
                                        </ul>
                                        <div class="tab-content">
                                            <div class="tab-pane fade show active" id="details" role="tabpanel">
                                                <p>{{$Ads[0]['description']}}</p>
                                            </div>
                                            {{-- <div class="tab-pane fade" id="features" role="tabpanel">
                                                <div class="item-features">
                                                    <ul class="item-meta">
                                                        <li><span>Device Type :</span> Laptop</li>
                                                        <li><span>Condition :</span> New</li>
                                                        <li><span>Model :</span> ZX-4356</li>
                                                    </ul>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <ul class="features-list">
                                                                <li>256GB PCI flash storage</li>
                                                                <li>Turbo Boost up to 3.1GHz</li>
                                                                <li>8GB memory (up from 4GB in 2013 model)</li>
                                                                <li>13.3" Retina Display</li>
                                                                <li>1 Year international warranty</li>
                                                            </ul>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <ul class="features-list">
                                                                <li>2.7 GHz dual-core Intel Core i5 processor</li>
                                                                <li>Intel Iris Graphics 6100</li>
                                                                <li>10 hour battery life</li>
                                                                <li>Intact Box</li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div> --}}
                                        </div>
                                    </div>
                                    <div class="item-action-area">
                                        <ul>
                                            <li class="inline-item"><a href="#"><i class="far fa-heart"></i>Add to Favourites</a></li>
                                            <li class="inline-item"><a href="#"><i class="fas fa-exclamation-triangle"></i>Report Abuse</a></li>
                                            <li class="item-social">
                                                <span class="share-title">
                                                    <i class="fas fa-share-alt"></i>
                                                    Share:
                                                </span>
                                                <a href="#" class="bg-facebook"><i class="fab fa-facebook-f"></i></a>
                                                <a href="#" class="bg-twitter"><i class="fab fa-twitter"></i></a>
                                                <a href="#" class="bg-google"><i class="fab fa-google-plus-g"></i></a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            {{-- <div class="item-google-map light-shadow-bg">
                                <h3 class="widget-border-title">Location</h3>
                                <div class="google-map light-box-content">
                                    <div id="googleMap" class="google-map" style="width: 100%; height: 460px;"></div>
                                </div>
                            </div> --}}
                            <div class="item-related-product light-shadow-bg">
                                <div class="flex-heading-layout2">
                                    <h3 class="widget-border-title">Related Ads</h3>
                                    <div id="owl-nav1" class="smart-nav-layout1">
                                        <span class="rt-prev">
                                            <i class="fas fa-angle-left"></i>
                                        </span>
                                        <span class="rt-next">
                                            <i class="fas fa-angle-right"></i>
                                        </span>
                                    </div>
                                </div>
                                <div class="light-box-content">
                                    <div class="rc-carousel" data-loop="true" data-items="4" data-margin="30" data-custom-nav="#owl-nav1" data-autoplay="false" data-autoplay-timeout="3000" data-smart-speed="1000" data-dots="false" data-nav="false" data-nav-speed="false" data-r-x-small="1" data-r-x-small-nav="false" data-r-x-small-dots="false" data-r-x-medium="2" data-r-x-medium-nav="false" data-r-x-medium-dots="false" data-r-small="2" data-r-small-nav="false" data-r-small-dots="false" data-r-medium="2" data-r-medium-nav="false" data-r-medium-dots="false" data-r-large="3" data-r-large-nav="false" data-r-large-dots="false" data-r-extra-large="3" data-r-extra-large-nav="false" data-r-extra-large-dots="false">
                                        <div class="product-box-layout1 box-shadwo-light mg-1">
                                            <div class="item-img">
                                                <a href="single-product1.html"><img src="media/product/product1.jpg" alt="Product"></a>
                                            </div>
                                            <div class="item-content">
                                                <h3 class="item-title"><a href="single-product1.html">HD 27 inch Mac 1 year used only</a><span>New</span></h3>
                                                <ul class="entry-meta">
                                                    <li><i class="far fa-clock"></i>4 months ago</li>
                                                    <li><i class="fas fa-map-marker-alt"></i>Port Chester, New York</li>
                                                </ul>
                                                <div class="item-price">
                                                    <span class="currency-symbol">$</span>
                                                    2,500
                                                </div>
                                            </div>
                                        </div>
                                        <div class="product-box-layout1 box-shadwo-light mg-1">
                                            <div class="item-img">
                                                <a href="single-product1.html"><img src="media/product/product2.jpg" alt="Product"></a>
                                            </div>
                                            <div class="item-content">
                                                <h3 class="item-title"><a href="single-product1.html">New Banded Smart Watch</a><span>New</span></h3>
                                                <ul class="entry-meta">
                                                    <li><i class="far fa-clock"></i>4 months ago</li>
                                                    <li><i class="fas fa-map-marker-alt"></i>Kansas, Emporia</li>
                                                </ul>
                                                <div class="item-price">
                                                    <span class="currency-symbol">$</span>
                                                    47
                                                </div>
                                            </div>
                                        </div>
                                        <div class="product-box-layout1 box-shadwo-light mg-1">
                                            <div class="item-img">
                                                <a href="single-product1.html" class="item-trending"><img src="media/product/product3.jpg" alt="Product"></a>
                                            </div>
                                            <div class="item-content">
                                                <h3 class="item-title"><a href="single-product1.html">High quality brand new headphone</a><span>New</span></h3>
                                                <ul class="entry-meta">
                                                    <li><i class="far fa-clock"></i>4 months ago</li>
                                                    <li><i class="fas fa-map-marker-alt"></i>New York, Mineola</li>
                                                </ul>
                                                <div class="item-price">
                                                    <span class="currency-symbol">$</span>
                                                    15
                                                </div>
                                            </div>
                                        </div>
                                        <div class="product-box-layout1 box-shadwo-light mg-1">
                                            <div class="item-img">
                                                <a href="single-product1.html"><img src="media/product/product4.jpg" alt="Product"></a>
                                            </div>
                                            <div class="item-content">
                                                <h3 class="item-title"><a href="single-product1.html">Running shoes imported from china</a><span>New</span></h3>
                                                <ul class="entry-meta">
                                                    <li><i class="far fa-clock"></i>4 months ago</li>
                                                    <li><i class="fas fa-map-marker-alt"></i>California, Bakersfield</li>
                                                </ul>
                                                <div class="item-price">
                                                    <span class="currency-symbol">$</span>
                                                    36
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                           {{--  <div class="item-review light-shadow-bg">
                                <h3 class="widget-border-title">Leave a Review</h3>
                                <div class="light-box-content">
                                    <form action="#">
                                        <div class="item-text">Your email address will not be published. Required fields are marked *</div>
                                        <div class="form-group">
                                            <label>Your Name *</label>
                                            <input type="text" class="form-control" name="name">
                                        </div>
                                        <div class="form-group">
                                            <label>Your Email *</label>
                                            <input type="email" class="form-control" name="email">
                                        </div>
                                        <div class="form-group">
                                            <label>Review Title *</label>
                                            <input type="text" class="form-control" name="review">
                                        </div>
                                        <div class="form-group item-rating">
                                            <label>Your Rating *</label>
                                            <ul>
                                                <li><a href="#"><i class="far fa-star"></i></a></li>
                                                <li><a href="#"><i class="far fa-star"></i></a></li>
                                                <li><a href="#"><i class="far fa-star"></i></a></li>
                                                <li><a href="#"><i class="far fa-star"></i></a></li>
                                                <li><a href="#"><i class="far fa-star"></i></a></li>
                                            </ul>
                                        </div>
                                        <div class="form-group">
                                            <label>Your Review *</label>
                                            <textarea class="form-control textarea" name="message" id="message" cols="30" rows="8"></textarea>
                                        </div>
                                        <div class="form-group">
                                            <button type="submit" class="item-btn">
                                                SUBMIT
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div> --}}
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-4 sidebar-break-md sidebar-widget-area">
                        <div class="widget-lg widget-price">
                            <div class="item-price">
                                <span class="currncy-symbol">₹</span>
                                {{$Ads[0]['price']}}
                            </div>
                        </div>
                        <div class="widget-lg widget-author-info widget-light-bg">
                            <h3 class="widget-border-title">Seller Information</h3>
                            <div class="author-content">
                                <div class="author-name">
                                    <div class="item-img">
                                        <img src="{{ url('default/User-default-image-boy.png') }}" alt="author">
                                    </div>
                                    {{-- <h4 class="author-title"><a href="store-detail.html">RadiusTheme</a></h4> --}}
                                </div>
                                <div class="author-meta">
                                    <ul>
                                        <li><i class="fas fa-map-marker-alt"></i>{{$key['city']}}, {{$key['state']}}</li>
                                        {{-- <li><i class="fas fa-shopping-basket"></i><a href="store-detail.html">View Store</a></li> --}}
                                    </ul>
                                </div>
                                <div class="phone-number classima-phone-reveal not-revealed" data-phone="{{$Ads[0]['user']['phone']}}">
                                    <div class="number"><i class="fas fa-phone"></i><span>XXXXXXXXXX</span></div>
                                    <div class="item-text">Click to reveal phone number</div>
                                </div>
                                <div class="author-mail">
                                    <a href="#" class="mail-btn" data-toggle="modal" data-target="#author-mail">
                                        <i class="fas fa-envelope"></i>Email to Seller
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="widget-lg widget-safty-tip widget-light-bg">
                            <h3 class="widget-border-title">Safety Tips for Buyers</h3>
                            <div class="safty-tip-content">
                                <ul>
                                    <li>Meet seller at a public place</li>
                                    <li>Check The item before you buy</li>
                                    <li>Pay only after collecting The item</li>
                                </ul>
                            </div>
                        </div>
                        <div class="widget widget-banner">
                            <a href="#">
                                <img src="media/figure/widget-banner1.jpg" alt="banner">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </section>

         <div class="modal fade author-mail-box" id="author-mail" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form id="contact-form">
                            <div class="form-group">
                                <input type="text" placeholder="Name *" class="form-control" name="name" data-error="Name field is required" required>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <input type="email" placeholder="Email *" class="form-control" name="email" data-error="email field is required" required>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <input type="text" placeholder="Subject *" class="form-control" name="subject" data-error="Subject field is required" required>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <textarea placeholder="Message *" class="textarea form-control" name="message" id="form-message" rows="4" cols="20" data-error="Message field is required" required></textarea>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="submit-btn">SUBMIT</button>
                            </div>
                            <div class="form-response"></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
@include('layout.footer')
