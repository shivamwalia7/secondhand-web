@include('layout.header')
       {{--  <section class="inner-page-banner" data-bg-image="media/banner/banner1.jpg">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="breadcrumbs-area">
                            <h1>My Account</h1>
                            <ul>
                                <li>
                                    <a href="index.html">Home</a>
                                </li>
                                <li>My Account</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </section> --}}
        <!--=====================================-->
        <!--=        Account Page Start      	=-->
        <!--=====================================-->
        <section class="section-padding-equal-70">
            <div class="container">
                <div class="row">
            
                    <div class="col-lg-12">
                       
                           
                             
                         
                            <div>
                                <div class="myaccount-login-form light-shadow-bg">
                                    <div class="light-box-content">
                                        <div class="row">
                                        	 <div class="col-lg-4">
                                                
                                            </div>
                                            <div class="col-lg-4">
                                                 <div class="form-box registration-form">
                                                    <h3 class="item-title">Register</h3>
                                                    <form method="post" action="{{ url('post-registration') }}">
                                                       {{ csrf_field() }}
                                                        <div class="form-group">
                                                            <label>first_name *</label>
                                                            <input type="text" class="form-control" name="first_name" id="registration-username">
                                                             @if ($errors->has('first_name'))
                  <span class="error">{{ $errors->first('first_name') }}</span>
                  @endif
                                                           
                                                        </div>
                                                        <div class="form-group">
                                                            <label>last_name *</label>
                                                            <input type="text" class="form-control" name="last_name" id="registration-username">
                                                             @if ($errors->has('last_name'))
                  <span class="error">{{ $errors->first('last_name') }}</span>
                  @endif
                                                           
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Email address *</label>
                                                            <input type="email" class="form-control" name="email" >
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Password *</label>
                                                            <input type="password" class="form-control" name="password" >
                                                        </div>
                                                        <div class="form-group">
                                                            <input type="submit" class="submit-btn" value="Register">
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                       
                   
                </div>
            </div>
        </section>
@include('layout.footer')
