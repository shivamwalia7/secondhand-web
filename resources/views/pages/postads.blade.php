@include('layout.header')

<section class="section-padding-equal-70">
            <div class="container">
                <div class="post-ad-box-layout1 light-shadow-bg">
                    <div class="post-ad-form light-box-content">
                        {{-- <div class="post-alert alert alert-success">You have more 958 free ads.</div> --}}
                        @if(session('success'))
                        <div class="alert alert-success">
                            {{session('success')}}
                        </div>
                    @endif
                        <form action="{{ url('postad') }}" method="post" enctype="multipart/form-data">
                                                        {{ csrf_field() }}

                           {{--  <div class="post-section post-type">
                                <div class="post-ad-title">
                                    <i class="fas fa-tags"></i>
                                    <h3 class="item-title">Select Type</h3>
                                </div>
                                <div class="row">
                                    <div class="col-sm-3">
                                        <label class="control-label">
                                            Ad Type
                                            <span>*</span>
                                        </label>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="form-group">
                                            <select class="form-control select-box">
                                                <option value="0">--Select Type--</option>
                                                <option value="1">Sell</option>
                                                <option value="2">Buy</option>
                                                <option value="3">Exchange</option>
                                                <option value="4">Job</option>
                                                <option value="5">To-Let</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div> --}}
                            <div class="post-section post-category">
                                <div class="post-ad-title">
                                    <i class="fas fa-tags"></i>
                                    <h3 class="item-title">Select Category</h3>
                                </div>
                                <div class="row">
                                    <div class="col-sm-3">
                                        <label class="control-label">
                                            Category
                                            <span>*</span>
                                        </label>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="form-group">
                                            <select name="category" class="form-control select-box">
                                                <option selected disabled>Select Category</option>
                                                @foreach($categories as $key => $value)
                                                    <option value="{{$key}}">{{$value}}</option>
                                                @endforeach
                                            </select>
                                            @if ($errors->has('category'))
                                              <span class="error" style="color: red;">{{ $errors->first('category') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-3">
                                        <label class="control-label">
                                            Sub Category
                                            <span>*</span>
                                        </label>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="form-group">
                                            <select name="subcategory" class="form-control select-box">
                                              <option selected disabled>Select Sub Category</option>
                                            </select>
                                            @if ($errors->has('subcategory'))
                                              <span class="error" style="color: red;">{{ $errors->first('subcategory') }}</span>
                                            @endif

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="post-section post-information">
                                <div class="post-ad-title">
                                    <i class="fas fa-folder-open"></i>
                                    <h3 class="item-title">Product Information</h3>
                                </div>
                                <div class="row">
                                    <div class="col-sm-3">
                                        <label class="control-label">
                                            Title
                                            <span>*</span>
                                        </label>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="form-group">
                                            <input type="text" value="{{old('title')}}" class="form-control" name="title" id="post-title">
                                            @if ($errors->has('title'))
                                              <span class="error" style="color: red;">{{ $errors->first('title') }}</span>
                                            @endif
                                        </div>

                                    </div>
                                </div>
                               {{--  <div class="row">
                                    <div class="col-sm-3">
                                        <label class="control-label">
                                            Price Type
                                            <span>*</span>
                                        </label>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="form-group">
                                            <select class="form-control select-box">
                                                <option value="1">Fixed</option>
                                                <option value="2">Negotiable</option>
                                                <option value="3">On Call</option>
                                            </select>
                                        </div>
                                    </div>
                                </div> --}}
                                <div class="row">
                                    <div class="col-sm-3">
                                        <label class="control-label">
                                            Price
                                            <span>*</span>
                                        </label>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="price" value="{{old('price')}}" id="post-price">
                                            @if ($errors->has('price'))
                                              <span class="error"  style="color: red;">{{ $errors->first('price') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                               {{--  <div class="row">
                                    <div class="col-sm-3">
                                        <label class="control-label">
                                            Condition
                                        </label>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="form-group">
                                            <div class="form-check form-radio-btn">
                                                <input class="form-check-input" type="radio" id="exampleRadios1" name="exampleRadios1" value="new">
                                                <label class="form-check-label" for="exampleRadios1">
                                                    New
                                                </label>
                                            </div>
                                            <div class="form-check form-radio-btn">
                                                <input class="form-check-input" type="radio" id="exampleRadios2" name="exampleRadios1" value="used">
                                                <label class="form-check-label" for="exampleRadios2">
                                                    Used
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div> --}}
                              {{--   <div class="row">
                                    <div class="col-sm-3">
                                        <label class="control-label">
                                            Brand
                                            <span>*</span>
                                        </label>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="form-group">
                                            <select class="form-control select-box">
                                                <option value="0">- Select an Option -</option>
                                                <option value="1">Apple</option>
                                                <option value="2">Samsung</option>
                                                <option value="3">Sony</option>
                                                <option value="4">LG</option>
                                                <option value="5">Acer</option>
                                                <option value="6">BenQ</option>
                                                <option value="7">Asus</option>
                                                <option value="8">Blackberry</option>
                                                <option value="9">Google Nexus</option>
                                                <option value="10">HTC</option>
                                                <option value="11">Huawei</option>
                                                <option value="12">Lenovo</option>
                                                <option value="13">Motorola</option>
                                                <option value="14">Nokia</option>
                                                <option value="15">Sony Ericsson</option>
                                                <option value="15">HP</option>
                                                <option value="15">Other Brand</option>
                                            </select>
                                        </div>
                                    </div>
                                </div> --}}
                                {{-- <div class="row">
                                    <div class="col-sm-3">
                                        <label class="control-label">
                                            Features
                                            <span>*</span>
                                        </label>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="form-group">
                                            <div class="form-check form-check-box">
                                                <input class="form-check-input" type="checkbox" id="features-list1" value="camera">
                                                <label class="form-check-label" for="features-list1">Camera</label>
                                            </div>
                                            <div class="form-check form-check-box">
                                                <input class="form-check-input" type="checkbox" id="features-list2" value="touch_screen">
                                                <label class="form-check-label" for="features-list2">Touch Screen</label>
                                            </div>
                                            <div class="form-check form-check-box">
                                                <input class="form-check-input" type="checkbox" id="features-list3" value="3g">
                                                <label class="form-check-label" for="features-list3">3G</label>
                                            </div>
                                            <div class="form-check form-check-box">
                                                <input class="form-check-input" type="checkbox" id="features-list4" value="4g">
                                                <label class="form-check-label" for="features-list4">4G</label>
                                            </div>
                                            <div class="form-check form-check-box">
                                                <input class="form-check-input" type="checkbox" id="features-list5" value="bluetooth">
                                                <label class="form-check-label" for="features-list5">Bluetooth</label>
                                            </div>
                                            <div class="form-check form-check-box">
                                                <input class="form-check-input" type="checkbox" id="features-list6" value="dual_sim">
                                                <label class="form-check-label" for="features-list6">Dual Sim</label>
                                            </div>
                                            <div class="form-check form-check-box">
                                                <input class="form-check-input" type="checkbox" id="features-list7" value="dual_lens_camera">
                                                <label class="form-check-label" for="features-list7">Dual Lens Camera</label>
                                            </div>
                                            <div class="form-check form-check-box">
                                                <input class="form-check-input" type="checkbox" id="features-list8" value="expandable_memory">
                                                <label class="form-check-label" for="features-list8">Expandable Memory</label>
                                            </div>
                                            <div class="form-check form-check-box">
                                                <input class="form-check-input" type="checkbox" id="features-list9" value="fingerprint_sensor">
                                                <label class="form-check-label" for="features-list9">Fingerprint Sensor</label>
                                            </div>
                                        </div>
                                    </div>
                                </div> --}}
                                {{-- <div class="row">
                                    <div class="col-sm-3">
                                        <label class="control-label">
                                            Authenticity
                                            <span>*</span>
                                        </label>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="form-group">
                                            <div class="form-check form-radio-btn">
                                                <input class="form-check-input" type="radio" id="exampleRadios3" name="exampleRadios2" value="original">
                                                <label class="form-check-label" for="exampleRadios3">
                                                    Original
                                                </label>
                                            </div>
                                            <div class="form-check form-radio-btn">
                                                <input class="form-check-input" type="radio" id="exampleRadios4" name="exampleRadios2" value="copy">
                                                <label class="form-check-label" for="exampleRadios4">
                                                    Copy
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div> --}}
                                <div class="row">
                                    <div class="col-sm-3">
                                        <label class="control-label">
                                            Description
                                        </label>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="form-group">
                                            <textarea name="description" class="form-control textarea" id="description" cols="30" rows="8">{{old('description')}}</textarea>
                                            @if ($errors->has('description'))
                                              <span class="error" style="color: red;">{{ $errors->first('description') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                           {{--  <div class="post-section post-features">
                                <div class="post-ad-title">
                                    <i class="fas fa-list-ul"></i>
                                    <h3 class="item-title">Features</h3>
                                </div>
                                <div class="row">
                                    <div class="col-sm-3">
                                        <label class="control-label">
                                            Features List
                                        </label>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="form-group">
                                            <textarea name="description" class="form-control textarea" id="description2" cols="30" rows="5"></textarea>
                                            <div class="help-text">
                                                <span>Write a feature in each line eg.</span>
                                                <span>Feature 1</span>
                                                <span>Feature 2</span>
                                                <span>....</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div> --}}
                            <div class="post-section post-img">
                                <div class="post-ad-title">
                                    <i class="far fa-image"></i>
                                    <h3 class="item-title">Images</h3>
                                </div>
                                <div class="form-group">
                                    <div class="img-gallery">
                                        <div class="img-upload">
                                            <div class="upload-title">Drop files here to add them.</div>
                                            {{-- <a href="#" class="item-btn">Browse files ...</a> --}}
                                            <input class="item-btn" type="file" name="image[]" multiple>
                                            @if ($errors->has('image'))
                                              <span class="error" style="color: red;">{{ $errors->first('image') }}</span>
                                            @endif
                                        </div>
                                       {{--  <div class="img-upload-instruction alert alert-danger">
                                            <p>Recommended image size to (870x493)px</p>
                                            <p>Image maximum size 3 MB.</p>
                                            <p>Allowed image type (png, jpg, jpeg).</p>
                                            <p>You can upload up to 5 images.</p>
                                        </div> --}}
                                    </div>
                                </div>
                            </div>
                            <div class="post-section post-contact">
                                <div class="post-ad-title">
                                    <i class="fas fa-user"></i>
                                    <h3 class="item-title">Contact Details</h3>
                                </div>
                                 <div class="row">
                                    <div class="col-sm-3">
                                        <label class="control-label">
                                            Name
                                        </label>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="form-group">
                                            <input disabled type="text"  class="form-control" name="" id="" value="{{$user->first_name}} {{$user->last_name}}">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-3">
                                        <label class="control-label">
                                            State
                                            <span>*</span>
                                        </label>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="form-group">
                                            <select name="state" class="form-control select-box">
                                                <option selected disabled>Select State</option>
                                                <option value="Punjab">Punjab</option>
                                              
                                            </select>
                                            @if ($errors->has('state'))
                                              <span class="error" style="color: red;">{{ $errors->first('state') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-3">
                                        <label class="control-label">
                                            City
                                            <span>*</span>
                                        </label>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="form-group">
                                            <select name="city" class="form-control select-box">
                                                <option selected disabled>Select City</option>

                                                <option value="Amritsar">Amritsar</option>
                                                <option value="Jalandhar">Jalandhar</option>
                                                >
                                            </select>
                                            @if ($errors->has('city'))
                                              <span class="error" style="color: red;">{{ $errors->first('city') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                {{-- <div class="row">
                                    <div class="col-sm-3">
                                        <label class="control-label">
                                            Zip Code
                                        </label>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="form-group">
                                            <input type="text" value="201301" class="form-control" name="zipcode" id="post-zip">
                                        </div>
                                    </div>
                                </div> --}}
                                <div class="row">
                                    <div class="col-sm-3">
                                        <label class="control-label">
                                            Address(Nearby)
                                        </label>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="form-group">
                                            <textarea name="address" class="form-control textarea"  id="address" cols="30" rows="2">{{old('address')}}</textarea>
                                            @if ($errors->has('address'))
                                              <span class="error" style="color: red;">{{ $errors->first('address') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                               
                               {{--  <div class="row">
                                    <div class="col-sm-3">
                                        <label class="control-label">
                                            Whatsapp Number
                                        </label>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="form-group">
                                            <input type="text" value="09985434436" class="form-control" name="whatsapp" id="whatsapp">
                                        </div>
                                    </div>
                                </div> --}}
                              {{--   <div class="row">
                                    <div class="col-sm-3">
                                        <label class="control-label">
                                            Email
                                        </label>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="form-group">
                                            <input type="email" value="radiustheme@gmail.com" class="form-control" name="email" id="email">
                                        </div>
                                    </div>
                                </div> --}}
                       {{--          <div class="row">
                                    <div class="col-sm-3">
                                        <label class="control-label">
                                            Map
                                        </label>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="form-group">
                                            <div id="googleMap" class="google-map" style="width: 100%; height: 400px;"></div>
                                            <div class="form-check form-check-box">
                                                <input class="form-check-input" type="checkbox" id="map">
                                                <label class="form-check-label" for="map">Don't show the Map</label>
                                            </div>
                                        </div>
                                    </div>
                                </div> --}}
                                <div class="row">
                                    <div class="col-sm-3">

                                    </div>
                                    <div class="col-sm-9">
                                        <div class="form-group">
                                            <input type="submit" class="submit-btn" value="POST NOW">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
        <script src="{{asset('assets/js/jquery.js')}}"></script>

    <script type="text/javascript">
    jQuery(document).ready(function ()
    {
            jQuery('select[name="category"]').on('change',function(){
               var ID = jQuery(this).val();
               console.log(ID);
               if(ID)
               {
                  jQuery.ajax({
                     url : 'getsubcategories/' +ID,
                     type : "GET",
                     dataType : "json",
                     success:function(data)
                     {
                        console.log(data);
                        jQuery('select[name="subcategory"]').empty();
                        jQuery.each(data, function(key,value){
                           $('select[name="subcategory"]').append('<option value="'+ key +'">'+ value +'</option>');
                        });
                     }
                  });
               }
               else
               {
                  $('select[name="subcategory"]').empty();
               }
            });
    });
    </script>
@include('layout.footer')
