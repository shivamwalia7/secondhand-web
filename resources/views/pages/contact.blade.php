@include('layout.header')
        <!--=====================================-->
        <!--=        Inner Banner Start         =-->
        <!--=====================================-->
        <section class="inner-page-banner" data-bg-image="media/banner/banner1.jpg">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="breadcrumbs-area">
                            <h1>Contact</h1>
                            <ul>
                                <li>
                                    <a href="index.html">Home</a>
                                </li>
                                <li>Contact</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--=====================================-->
        <!--=        Contact Page Start    		=-->
        <!--=====================================-->
        <section class="section-padding-equal-70">
            <div class="container">
                <div class="contact-page-box-layout1 light-shadow-bg">
                    <div class="light-box-content">
                        <div id="googleMap" class="google-map" style="width: 100%; height: 415px;"></div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="contact-info">
                                    <h3 class="item-title">Information</h3>
                                    <ul>
                                        <li><i class="fas fa-paper-plane"></i>PO Box 16122 Collins Street West Victoria 8007 Australia</li>
                                        <li><i class="fas fa-phone-volume"></i>+1238884444</li>
                                        <li><i class="far fa-envelope"></i>info@example.com</li>
                                        <li><i class="fas fa-fax"></i>+1234657890</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="contact-form-box">
                                    <h3 class="item-title">Send Us A Message</h3>
                                    <form id="contact-form">
                                        <div class="form-group">
                                            <input type="text" placeholder="Name *" class="form-control" name="name" data-error="Name field is required" required>
                                            <div class="help-block with-errors"></div>
                                        </div>
                                        <div class="form-group">
                                            <input type="email" placeholder="Email *" class="form-control" name="email" data-error="email field is required" required>
                                            <div class="help-block with-errors"></div>
                                        </div>
                                        <div class="form-group">
                                            <input type="text" placeholder="Subject *" class="form-control" name="subject" data-error="Subject field is required" required>
                                            <div class="help-block with-errors"></div>
                                        </div>
                                        <div class="form-group">
                                            <textarea placeholder="Message *" class="textarea form-control" name="message" id="form-message" rows="8" cols="20" data-error="Message field is required" required></textarea>
                                            <div class="help-block with-errors"></div>
                                        </div>
                                        <div class="form-group">
                                            <button type="submit" class="submit-btn">SUBMIT</button>
                                        </div>
                                        <div class="form-response"></div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!--=====================================-->
        <!--=       Footer Start           	=-->
        <!--=====================================-->
@include('layout.footer')