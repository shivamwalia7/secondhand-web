        
<link rel="stylesheet" href="{{asset('dependencies/bootstrap/css/bootstrap.min.css')}}">
    {{-- <link rel="stylesheet" href="{{asset('dependencies/fontawesome/css/all.min.css')}}"> --}}
    <link rel="stylesheet" href="{{asset('assets/css/app.css')}}">

        <section class="section-padding-equal-70 bg-accent">
            <div class="container">
                <div class="error-page-box-layout1">
                    <div class="item-img">
                        <img src="media/figure/404.png" alt="404">
                    </div>
                    <h2 class="item-title">We can't find the page you're looking for</h2>
                    <a href="{{ url('/') }}" class="error-btn">Go To Home Page</a>
                </div>
            </div>
        </section>