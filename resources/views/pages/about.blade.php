@include('layout.header')
        <!--=====================================-->
        <!--=        Inner Banner Start         =-->
        <!--=====================================-->
        <section class="inner-page-banner" data-bg-image="media/banner/banner1.jpg">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="breadcrumbs-area">
                            <h1>About Us</h1>
                            <ul>
                                <li>
                                    <a href="index.html">Home</a>
                                </li>
                                <li>About Us</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--=====================================-->
        <!--=          About Us Start        	=-->
        <!--=====================================-->
        <section class="about-wrap-layout1">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="about-box-layout1">
                            <h2 class="item-title">About Our Company</h2>
                            <p>Tmply dummy text of the printing and typesetting indust Lorem Ipsum has been theitry’s snce simply dummy text of the printing.Phasellus enim libero, blandit vel sapien vitae, condimentum ultricies magna euismod.Tmply dummy text of the printing and typesetting indust Lorem Ipsum has been their.</p>
                            <h3 class="item-title">What We Do</h3>
                            <p>Tmply dummy text of the printing and typesetting indust Lorem Ipsum has been theitry’s snce simply dummy text of the printing.Phasellus enim libero, blandit vel sapien vitae, condimentum ultricies magna euismod.Tmply dummy text of the printing and typesetting indust Lorem Ipsum has been their.</p>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="about-box-layout2">
                            <div class="item-img">
                                <img src="media/about/about1.jpg" alt="About Us">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--=====================================-->
        <!--=       Process Start           	=-->
        <!--=====================================-->
        <section class="section-padding-top-heading bg-accent">
            <div class="container">
                <div class="heading-layout1">
                    <h2 class="heading-title">How it Works</h2>
                </div>
                <div class="row gutters-20">
                    <div class="col-lg-3 col-sm-6">
                        <div class="process-box-layout2">
                            <div class="item-icon">
                                <i class="fas fa-user"></i>
                            </div>
                            <div class="item-content">
                                <h3 class="item-title">Create Account</h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing
                                    elit aliquam consectetur sit amet ante nec vulputate</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <div class="process-box-layout2">
                            <div class="item-icon">
                                <i class="fas fa-pencil-alt"></i>
                            </div>
                            <div class="item-content">
                                <h3 class="item-title">Post your Ad</h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing
                                    elit aliquam consectetur sit amet ante nec vulputate</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <div class="process-box-layout2">
                            <div class="item-icon">
                                <i class="fas fa-volume-up"></i>
                            </div>
                            <div class="item-content">
                                <h3 class="item-title">Get Offers</h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing
                                    elit aliquam consectetur sit amet ante nec vulputate</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <div class="process-box-layout2">
                            <div class="item-icon">
                                <i class="fas fa-dollar-sign"></i>
                            </div>
                            <div class="item-content">
                                <h3 class="item-title">Sell Your Item</h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing
                                    elit aliquam consectetur sit amet ante nec vulputate</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--=====================================-->
        <!--=       Counter Start           	=-->
        <!--=====================================-->
        <section class="counter-wrap-layout1 bg-attachment-fixed" data-bg-image="media/figure/counter-bg.jpg">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 d-flex justify-content-center">
                        <div class="counter-box-layout1">
                            <div class="item-icon">
                                <i class="flaticon-gift"></i>
                            </div>
                            <div class="item-content">
                                <div class="counter-number">
                                    <span class="counter">5000</span>
                                    <span>+</span>
                                </div>
                                <div class="item-title">Published Ads</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 d-flex justify-content-center">
                        <div class="counter-box-layout1">
                            <div class="item-icon">
                                <i class="flaticon-users"></i>
                            </div>
                            <div class="item-content">
                                <div class="counter-number">
                                    <span class="counter">3265</span>
                                    <span>+</span>
                                </div>
                                <div class="item-title">Registered Users</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 d-flex justify-content-center">
                        <div class="counter-box-layout1">
                            <div class="item-icon">
                                <i class="flaticon-shield"></i>
                            </div>
                            <div class="item-content">
                                <div class="counter-number">
                                    <span class="counter">2000</span>
                                    <span>+</span>
                                </div>
                                <div class="item-title">Verified Users</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--=====================================-->
        <!--=       Counter Start           	=-->
        <!--=====================================-->
        <section class="section-padding-top-heading bg-accent">
            <div class="container">
                <div class="heading-layout1">
                    <h2 class="heading-title">Customers Say About Us</h2>
                </div>
                <div class="row gutters-20">
                    <div class="col-md-4">
                        <div class="testimonial-box-layout1">
                            <p>Got the products delivered in our doorstep quickly,
                                the customer support was super helpful and they answered
                                all my queries in time. Highly recommended!</p>
                            <div class="item-img">
                                <img src="media/figure/testimonial1.jpg" alt="testimonial">
                            </div>
                            <h3 class="item-title">David Lee</h3>
                            <div class="item-designation">CEO, TechHb</div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="testimonial-box-layout1">
                            <p>Got the products delivered in our doorstep quickly,
                                the customer support was super helpful and they answered
                                all my queries in time. Highly recommended!</p>
                            <div class="item-img">
                                <img src="media/figure/testimonial2.jpg" alt="testimonial">
                            </div>
                            <h3 class="item-title">Tom Steven</h3>
                            <div class="item-designation">Freelance Designer</div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="testimonial-box-layout1">
                            <p>Got the products delivered in our doorstep quickly,
                                the customer support was super helpful and they answered
                                all my queries in time. Highly recommended!</p>
                            <div class="item-img">
                                <img src="media/figure/testimonial3.jpg" alt="testimonial">
                            </div>
                            <h3 class="item-title">Mike Hussey</h3>
                            <div class="item-designation">Journalist, NewAge</div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--=====================================-->
        <!--=          Brnad Start        		=-->
        <!--=====================================-->
        <section class="brand-wrap-layout1">
            <div class="container">
                <div class="rc-carousel" data-loop="true" data-items="10" data-margin="30" data-autoplay="true" data-autoplay-timeout="3000" data-smart-speed="1000" data-dots="false" data-nav="true" data-nav-speed="false" data-r-x-small="1" data-r-x-small-nav="false" data-r-x-small-dots="false" data-r-x-medium="2" data-r-x-medium-nav="false" data-r-x-medium-dots="false" data-r-small="3" data-r-small-nav="false" data-r-small-dots="false" data-r-medium="3" data-r-medium-nav="false" data-r-medium-dots="false" data-r-large="4" data-r-large-nav="false" data-r-large-dots="false" data-r-extra-large="4" data-r-extra-large-nav="false" data-r-extra-large-dots="false">
                    <div class="brand-box-layout1">
                        <img src="media/brand/brand1.jpg" alt="brand">
                    </div>
                    <div class="brand-box-layout1">
                        <img src="media/brand/brand2.jpg" alt="brand">
                    </div>
                    <div class="brand-box-layout1">
                        <img src="media/brand/brand3.jpg" alt="brand">
                    </div>
                    <div class="brand-box-layout1">
                        <img src="media/brand/brand4.jpg" alt="brand">
                    </div>
                </div>
            </div>

        </section>
        <!--=====================================-->
        <!--=       Footer Start           	=-->
        <!--=====================================-->

@include('layout.footer')

