@include('layout.header')
        <section class="inner-page-banner" data-bg-image="media/banner/banner1.jpg">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="breadcrumbs-area">
                            <h1>My Account</h1>
                            <ul>
                                <li>
                                    <a href="index.html">Home</a>
                                </li>
                                <li>My Account</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--=====================================-->
        <!--=        Account Page Start      	=-->
        <!--=====================================-->
        <section class="section-padding-equal-70">
            <div class="container">
                <div class="row">
            
                    <div class="col-lg-12">
                       
                           
                             
                         
                            <div>
                                <div class="myaccount-login-form light-shadow-bg">
                                    <div class="light-box-content">
                                        @if(session('success'))
                        <div class="alert alert-success">
                            {{session('success')}}
                        </div>
                    @endif
                                        <div class="row">

                                        	 <div class="col-lg-4">
                                                
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="form-box login-form">
                                                    <h3 class="item-title">Login</h3>
                                                    <form method="post"  action="{{ url('post-login') }}">
                                                        {{ csrf_field() }}
                                                        <div class="form-group">
                                                            <label>E-mail</label>
                                                            <input type="text" class="form-control" name="email" id="login-username">
                                                            @if ($errors->has('email'))
                  <span class="error" style="color: red;">{{ $errors->first('email') }}</span>
                  @endif
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Password</label>
                                                            <input type="password" class="form-control" name="password" id="login-password">
                                                            @if ($errors->has('password'))
                  <span class="error" style="color: red;">{{ $errors->first('password') }}</span>
                  @endif
                                                        </div>
                                                        <div class="form-group d-flex">
                                                            <input type="submit" class="submit-btn" value="Login">
                                                            <div class="form-check form-check-box">
                                                                <input class="form-check-input" type="checkbox" id="check-password">
                                                                <label for="check-password">Remember Me</label>
                                                            </div>
                                                        </div>
                                                        {{-- <div class="form-group">
                                                            <a href="#" class="forgot-password">Forgot your password?</a>
                                                        </div> --}}

                                                        <div class="form-group">
                                                            <a href="{{ url('/registration') }}" class="forgot-password">Don't Have Account? Register Yourself</a>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                       
                   
                </div>
            </div>
        </section>
@include('layout.footer')

