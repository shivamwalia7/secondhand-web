<!DOCTYPE html>
<html class="no-js" lang="">


<!-- Mirrored from www.radiustheme.com/demo/html/classima/index5.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 06 May 2020 11:06:34 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>
    <!-- Meta Data -->
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Second Hand</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="media/favicon.png">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{asset('dependencies/bootstrap/css/bootstrap.min.css')}}">
    <!-- FontAwesome CSS -->
    <link rel="stylesheet" href="{{asset('dependencies/fontawesome/css/all.min.css')}}">
    <!-- Flaticon CSS -->
    <link rel="stylesheet" href="{{asset('dependencies/flaticon/flaticon.css')}}">
    <!-- Owl Carousel CSS -->
    <link rel="stylesheet" href="{{asset('dependencies/owl.carousel/css/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{asset('dependencies/owl.carousel/css/owl.theme.default.min.css')}}">
    <!-- Animated Headlines CSS -->
    <link rel="stylesheet" href="{{asset('dependencies/jquery-animated-headlines/css/jquery.animatedheadline.css')}}">
    <!-- Magnific Popup CSS -->
    <link rel="stylesheet" href="{{asset('dependencies/magnific-popup/css/magnific-popup.css')}}">
    <!-- Animate CSS -->
    <link rel="stylesheet" href="{{asset('dependencies/animate.css/css/animate.min.css')}}">
    <!-- Meanmenu CSS -->
    <link rel="stylesheet" href="{{asset('dependencies/meanmenu/css/meanmenu.min.css')}}">
    <!-- Site Stylesheet -->
    <link rel="stylesheet" href="{{asset('assets/css/app.css')}}">

    <!-- Google Web Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:300,300i,400,400i,600,600i,700,700i,800,800i&amp;display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i,900,900i&amp;display=swap" rel="stylesheet">

</head>

<body class="sticky-header">
    <!--[if lte IE 9]>
    <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
  	<![endif]-->
    <!-- ScrollUp Start Here -->
    <a href="#wrapper" data-type="section-switch" class="scrollup">
        <i class="fas fa-angle-double-up"></i>
    </a>
    <!-- ScrollUp End Here -->
    <!-- Preloader Start Here -->
    <div id="preloader"></div>
    <!-- Preloader End Here -->
    <div id="wrapper" class="wrapper">

        <!--=====================================-->
        <!--=            Header Start           =-->
        <!--=====================================-->
        <header class="header">
            <div id="rt-sticky-placeholder"></div>
            <div id="header-menu" class="header-menu menu-layout2">
                <div class="container">
                    <div class="row d-flex align-items-center">
                        <div class="col-lg-2">
                            <div class="logo-area">
                                <a href="{{ url('/') }}" class="temp-logo">
                                    <img src="{{ url('default/logo.png') }}" alt="logo" class="img-fluid">
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-7 d-flex justify-content-end">
                            <nav id="dropdown" class="template-main-menu">
                                <ul>
                                    <li>
                                        <a href="{{ url('/') }}" >Home</a>
                                     
                                    </li>
                                   {{--  <li>
                                        <a href="about.html" class="has-dropdown">All Ads</a>
                                        <ul class="mega-menu mega-menu-col-3">
                                            <li>
                                                <ul class="sub-menu">
                                                    <li>
                                                        <a href="grid-view1.html">Grid View 1</a>
                                                    </li>
                                                    <li>
                                                        <a href="grid-view2.html">Grid View 2</a>
                                                    </li>
                                                    <li>
                                                        <a href="grid-view3.html">Grid View 3</a>
                                                    </li>
                                                    <li>
                                                        <a href="grid-view4.html">Grid View 4</a>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li>
                                                <ul class="sub-menu">
                                                    <li>
                                                        <a href="list-view1.html">List View 1</a>
                                                    </li>
                                                    <li>
                                                        <a href="list-view2.html">List View 2</a>
                                                    </li>
                                                    <li>
                                                        <a href="list-view3.html">List View 3</a>
                                                    </li>
                                                    <li>
                                                        <a href="list-view4.html">List View 4</a>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li>
                                                <ul class="sub-menu">
                                                    <li>
                                                        <a href="single-product1.html">Single Ad 1</a>
                                                    </li>
                                                    <li>
                                                        <a href="single-product2.html">Single Ad 2</a>
                                                    </li>
                                                    <li>
                                                        <a href="store.html">All Stores</a>
                                                    </li>
                                                    <li>
                                                        <a href="store-detail.html">Store Details</a>
                                                    </li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </li> --}}
                                   {{--  <li>
                                        <a href="#" class="has-dropdown">Pages</a>
                                        <ul class="mega-menu mega-menu-col-2">
                                            <li>
                                                <ul class="sub-menu">
                                                    <li>
                                                        <a href="about.html">About Us</a>
                                                    </li>
                                                    <li>
                                                        <a href="faq.html">FAQ</a>
                                                    </li>
                                                    <li>
                                                        <a href="search-result.html">Search Result</a>
                                                    </li>
                                                    <li>
                                                        <a href="404.html">Error 404</a>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li>
                                                <ul class="sub-menu">
                                                    <li>
                                                        <a href="all-ad.html">All Ads</a>
                                                    </li>
                                                    <li>
                                                        <a href="single-product1.html">Single Ad 1</a>
                                                    </li>
                                                    <li>
                                                        <a href="single-product2.html">Single Ad 2</a>
                                                    </li>
                                                    <li>
                                                        <a href="account.html">My Account</a>
                                                    </li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </li> --}}
                                    <li>
                                        <a href="{{ url('/about') }}" >About Us</a>
                                        <ul class="sub-menu">
                                            <li>
                                                <a href="store.html">All Stores</a>
                                            </li>
                                            <li>
                                                <a href="store-detail.html">Store Details</a>
                                            </li>
                                        </ul>
                                    </li>
                                    {{-- <li>
                                        <a href="#" class="has-dropdown">Blog</a>
                                   
                                    </li> --}}
                                    <li>
                                        <a href="{{ url('/contact') }}">Contact</a>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                        <div class="col-lg-3 d-flex justify-content-end">
                            <div class="header-action-layout1">
                                <ul>
                                    @if(Auth::user()==null)
                                    <li class="header-login-icon">
                                        <a href="{{ url('/login') }}" class="color-primary" data-toggle="tooltip" data-placement="top" title="Login">
                                            <i class="far fa-user"></i>
                                        </a>
                                    </li>
                                </ul>
                                    @else
                            <nav id="dropdown" class="template-main-menu">

                                    <ul>
                                        <li>
                                        <a href="#" class="has-dropdown">Account</a>
                                        <ul class="sub-menu">
                                            <li>
                                                <a href="{{ url('myaccount') }}">My account</a>
                                            </li>
                                            <li>
                                                <a href="{{ url('logout') }}">Logout</a>
                                            </li>

                                           
                                        </ul>
                                    </li>
                                </ul>
                            </nav>

                                   
                                    @endif
                                   {{--  <li class="header-login-icon">
                                        <a href="{{ url('/register') }}" class="color-primary" data-toggle="tooltip" data-placement="top" title="Register">
                                            <i class="far fa-user"></i>
                                        </a>
                                    </li> --}}
                                    {{--  <li class="header-login-icon">
                                        <a href="{{ url('/myaccount') }}" class="color-primary" data-toggle="tooltip" data-placement="top" title="Account">
                                            <i class="far fa-user"></i>
                                        </a>
                                    </li> --}}
                                    <li class="header-btn">
                                        <a href="{{ url('/postad') }}" class="item-btn"><i class="fas fa-plus"></i>Post Your Ad</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>

        @yield('search')