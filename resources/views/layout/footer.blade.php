        <footer>
            <div class="footer-top-wrap">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-3 col-sm-6">
                            <div class="footer-box-layout1">
                                <div class="footer-logo">
                                    <img src="{{ url('default/logo.png') }}" alt="logo">
                                </div>
                                <p>Secondhandbazar is a Largest Classified Listing Marketplace
                                    offers.</p>
                                <ul class="footer-social">
                                    <li><a href="#" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
                                    <li><a href="#" target="_blank"><i class="fab fa-twitter"></i></a></li>
                                    <li><a href="#" target="_blank"><i class="fab fa-google-plus-g"></i></a></li>
                                    <li><a href="#" target="_blank"><i class="fab fa-linkedin-in"></i></a></li>
                                    <li><a href="#" target="_blank"><i class="fab fa-pinterest"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-3 col-sm-6">
                            <div class="footer-box-layout1">
                                <div class="footer-title">
                                    <h3>How to Sell Fast</h3>
                                </div>
                                <div class="footer-menu-box">
                                    <ul>
                                        <li><a href="#">Selling Tips</a></li>
                                        <li><a href="#">Buy and Sell Quickly</a></li>
                                        <li><a href="#">Membership</a></li>
                                        <li><a href="#">Banner Advertising</a></li>
                                        <li><a href="#">Promote Your Ad</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-sm-6">
                            <div class="footer-box-layout1">
                                <div class="footer-title">
                                    <h3>Information</h3>
                                </div>
                                <div class="footer-menu-box">
                                    <ul>
                                        <li><a href="#">Company & Contact Info</a></li>
                                        <li><a href="#">Blog & Articles</a></li>
                                        <li><a href="#">Sitemap</a></li>
                                        <li><a href="#">Terms of Service</a></li>
                                        <li><a href="#">Privacy Policy</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-sm-6">
                            <div class="footer-box-layout1">
                                <div class="footer-title">
                                    <h3>Help & Support</h3>
                                </div>
                                <div class="footer-menu-box">
                                    <ul>
                                        <li><a href="#">Live Chat</a></li>
                                        <li><a href="#">FAQ</a></li>
                                        <li><a href="#">How to Stay Safe</a></li>
                                        <li><a href="#">Terms & Conditions</a></li>
                                        <li><a href="#">Contact Us</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer-bottom-wrap">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="copyright-text">
                                © Copyright Secondhandbazar 2020<a target="_blank" href="#"></a>
                            </div>
                        </div>
                       {{--  <div class="col-md-4">
                            <div class="payment-option">
                                <a href="#">
                                    <img src="{{ url('media/figure/payment.png') }}" alt="payment">
                                </a>
                            </div>
                        </div> --}}
                    </div>
                </div>
            </div>
        </footer>

    </div>
    <!-- Jquery Js -->
    <script src="{{asset('dependencies/jquery/js/jquery.min.js')}}"></script>
    <!-- Popper Js -->
    <script src="{{asset('dependencies/popper.js/js/popper.min.js')}}"></script>
    <!-- Bootstrap Js -->
    <script src="{{asset('dependencies/bootstrap/js/bootstrap.min.js')}}"></script>
    <!-- Waypoints Js -->
    <script src="{{asset('dependencies/waypoints/js/jquery.waypoints.min.js')}}"></script>
    <!-- Counterup Js -->
    <script src="{{asset('dependencies/jquery.counterup/js/jquery.counterup.min.js')}}"></script>
    <!-- Owl Carousel Js -->
    <script src="{{asset('dependencies/owl.carousel/js/owl.carousel.min.js')}}"></script>
    <!-- ImagesLoaded Js -->
    <script src="{{asset('dependencies/imagesloaded/js/imagesloaded.pkgd.min.js')}}"></script>
    <!-- Isotope Js -->
    <script src="{{asset('dependencies/isotope-layout/js/isotope.pkgd.min.js')}}"></script>
    <!-- Animated Headline Js -->
    <script src="{{asset('dependencies/jquery-animated-headlines/js/jquery.animatedheadline.min.js')}}"></script>
    <!-- Magnific Popup Js -->
    <script src="{{asset('dependencies/magnific-popup/js/jquery.magnific-popup.min.js')}}"></script>
    <!-- ElevateZoom Js -->
    <script src="{{asset('dependencies/elevatezoom/js/jquery.elevateZoom-2.2.3.min.js')}}"></script>
    <!-- Bootstrap Validate Js -->
    <script src="{{asset('dependencies/bootstrap-validator/js/validator.min.js')}}"></script>
    <!-- Meanmenu Js -->
    <script src="{{asset('dependencies/meanmenu/js/jquery.meanmenu.min.js')}}"></script>
    <!-- Google Map js -->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBtmXSwv4YmAKtcZyyad9W7D4AC08z0Rb4"></script>
    <!-- Site Scripts -->
    <script src="{{asset('assets/js/app.js')}}"></script>

</body>


<!-- Mirrored from www.radiustheme.com/demo/html/classima/index5.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 06 May 2020 11:06:35 GMT -->
</html>