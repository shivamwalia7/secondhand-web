<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;
use App\Model\Categories;
use App\Model\SubCategories;
use App\Model\Ads;
use App\Model\AdsImages;
use App\Traits\ImageUpload;
use Carbon\Carbon;
use Hash;



class HomeController extends Controller
{
    use ImageUpload;


    public function index()
    {
        

        if(true)
        {
        $Ad=Ads::with('images','category')->get()->toArray();


        }

        else
        {
        $userId=Auth()->user()->id;
        $Ad=Ads::with('images','category')->where('posted_by',$userId)->get()->toArray();

        }
        $Ads=[];
        foreach ($Ad as $key) {
            $d=Carbon::parse($key['created_at'])->format('d-M-Y');
            $key['created_at']=$d;
            $Ads[]=$key;    
        }

        
    	return view('pages.index')->with('Ads',$Ads);
    }


    public function Search(Request $request)
    {   
        // dd($request);
                $location=$request->location;
                $keyword=$request->keyword;
        $Ad=Ads::with('images','category')->where('title', 'LIKE', "%{$keyword}%") 
  ->get()->toArray();


        $Ads=[];
        foreach ($Ad as $key) {
            $d=Carbon::parse($key['created_at'])->format('d-M-Y');
            $key['created_at']=$d;
            $Ads[]=$key;    
        }

        // dd($Ads);
        return view('pages.index')->with('Ads',$Ads);
    }


    public function about()
    {
    	return view('pages.about');
    
    }


    public function contact()
    {
    	return view('pages.contact');
    	
    }

    // public function login()
    // {
    // 	return view('pages.login');
    	
    // }

    // public function register()
    // {
    	

    //     return view('pages.register');
    	
    // }

    public function postads(Request $request)
    {
        
        if ($request->isMethod('post')) {
            // dd($request);
        
            $this->validate($request,[
                'category'=>'required',
                'subcategory'=>'required',
                'title'=>'required',
                'price'=>'required',
                'description'=>'required',
                'state'=>'required',
                'city'=>'required',
                'address'=>'required',
                
                ]);


            $posted_by=Auth()->user()->id;
            $PostAd= new Ads;
            $PostAd->category_id=$request->category;
            $PostAd->sub_category_id=$request->subcategory;
            $PostAd->posted_by=$posted_by;
            $PostAd->title=$request->title;
            $PostAd->price=$request->price;
            $PostAd->description=$request->description;
            $PostAd->city=$request->city;
            $PostAd->state=$request->state;
            $PostAd->address=$request->address;
            $PostAd->save();

            $PostId=$PostAd->id;

            if ($request->hasFile('image')) {
        foreach($request->file('image') as $file){
          $filePath = $this->UserImageUpload($file); //passing parameter to our trait method one after another using foreach loop
            AdsImages::create([
              'ad_id'=>$PostId,  
              'image' => $filePath,
            ]);
          }

        }


        return back()->with('success','Ad Successfully Posted');



        }


         $user=Auth()->user();

        $Categories=Categories::get()->pluck("name","id");
        
        return view('pages.postads')->with('categories',$Categories)->with('user',$user);
        
    }

    public function getSubcategoriesById($id)
    {
        $SubCategories=SubCategories::where('categories_id',$id)->get()->pluck("name","id");


        return json_encode($SubCategories);
    }

    public function myaccount()
    {   

        $user=Auth::user();
        // $user=User::where('id',$id)->get()->first();
        $Ad=Ads::with('images','category')->where('posted_by',$user->id)->get()->toArray();
        $Ads=[];
        foreach ($Ad as $key) {
            $d=Carbon::parse($key['created_at'])->format('d-M-Y');
            $key['created_at']=$d;
            $Ads[]=$key;
       }

        return view('pages.myaccount')->with('user',$user)->with('Ads',$Ads);
        
    }

    public function error()
    {
        return view('pages.404page');
        
    }


    public function DeleteAdByUser($adId)
    {
        $UserId=Auth::user()->id;
        Ads::where('posted_by',$UserId)->where('id',$adId)->delete();
        AdsImages::where('ad_id')->delete();
        // redirect('/myaccount#mylisting');

       return redirect()->back();


        // return back();
    }


    public function AdDetails($adId)
    {
         // $user=Auth::user();
        // $user=User::where('id',$id)->get()->first();
        $Ad=Ads::with('images','multipleimages','category','user')->where('id',$adId)->get()->toArray();
        $Ads=[];
        foreach ($Ad as $key) {
            $d=Carbon::parse($key['created_at'])->format('d-M-Y');
            $key['created_at']=$d;
            $Ads[]=$key;
       }

       // dd($Ads);


        return view('pages.ad-details')->with('Ads',$Ads);
    }


        public function UpdateProfile(Request $req)
    {
       

        $id = \Auth::user()->id;
        // $getConsumer = User::where('id',$id)->first();
        
        if ($req->isMethod('post')) {

            $this->validate($req,[
                'first_name'=>'required|alpha',
                'last_name'=>'required|alpha',
                'phone'=>'required',

            ]);

             $userDetail = User::find($id);
             $userDetail->first_name = $req->first_name;
             $userDetail->last_name = $req->last_name;
             $userDetail->phone = $req->phone;
             $userDetail->save();

             return back()->with('success','User Updated Successfully');
        }


        // return view('Consumer/UpdateProfile')->with('user',$getConsumer);
    }



    public function ChangePassword(Request $req)
    {
        if ($req->isMethod('post')) {
           // dd(Hash::make($req->newpassword));
            $this->validate($req,[
                'currentpassword'=>'required',
                'newpassword'=>'required|same:confirmpassword',
                'confirmpassword'=>'required',
            ]);
             $id = \Auth::user()->id;
             $password = \Auth::user()->password;
           
            if (Hash::check($req->currentpassword, $password)) {


                $consumerDetail = User::findOrFail($id);
                $consumerDetail->password = Hash::make($req->newpassword);
                $consumerDetail->save();
                return back()->with('success','Password changed Successfully');
            }else{
                return back()->with('error','Invalid Current Password');
            }
          }else{
           return back();
        }
       
    }

    public function ResetForm(Request $req)
{
   if ($req->isMethod('post')) {
            // validate 
            $this->validate($req,[
                'email' => 'required|email'
            ]);
             
            $user = User::where('email', $req->email)->where('is_deleted','0')->first();

            if (!empty($user->social_id)) {
               return back()->with('error','This Email is registered by Social Login');
            }else{

            if (!$user){
                return back()->with('error','We can`t find a user with that e-mail address');
            }
            $passwordReset = PasswordReset::updateOrCreate(['email' => $user->email],['email' => $user->email,'token' => \Str::random(60)]);
            if ($user && $passwordReset){
                $userPasswordReset = PasswordReset::whereemail($user->email)->first();
                 Mail::to($user->email)->send(new ResetPassword($user,$userPasswordReset));   
                return back()->with('success','We have e-mailed your password reset link!');
            }
          }
        }

  return view('Front/ResetPassword/ResetForm');
}




public function SendResetPasswordLink(Request $req)
{
    $token = $req->token;
    $passwordReset = PasswordReset::where('token', $token)->first();
            // dd($passwordReset);
        if (empty($passwordReset))
            return view('Front/ResetPassword/ResetPassword',compact("token"))->with('error',"This password reset token is invalid.");
        if (Carbon::parse($passwordReset->updated_at)->addMinutes(720)->isPast()) {
             
            $passwordReset->delete();
            return view('Front/ResetPassword/ResetPassword',compact("token"))->with('error',"This password reset token is invalid.");
        }
  
  return view('Front/ResetPassword/ResetPassword',compact("token"));
}




public function ResetPassword(Request $req)
{
        $req->validate(['email' => 'required|email',
                        'password' => 'required|confirmed'
                        ]);


                $passwordReset = PasswordReset::where(['token'=> $req->token,'email'=> $req->email])->first();
                
                if (!$passwordReset){
                    return back()->with('error',"This password reset token is invalid.");
                }

                $user = User::where('email', $passwordReset->email)->first();
                if (!$user){
                    return back()->with('error',"We can't find a user with that e-mail address.");
                }
                $user->password = bcrypt($req->password);
                if($user->save()){  
                    $passwordReset->delete();

                    return back()->with('success','Password Reset successfully');
                }else{
                    return back()->with('error',"Somthing went wrong");
                }

 }
}
