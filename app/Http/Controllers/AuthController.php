<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator,Redirect,Response;
Use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Session;

class AuthController extends Controller
{
    public function index()
    {
        return view('pages.login');
    	
    }


    public function registration()
    {
        return view('pages.register');
    	
    }

    public function postRegistration(Request $request )
    {
    	// dd($request);
    	 request()->validate([
        'first_name' => 'required',
        'last_name' => 'required',

        'email' => 'required|email|unique:users',
        'password' => 'required|min:6',
        ]);
         
        $data = $request->all();
 
        User::create([
        'first_name' => $data['first_name'],
        'last_name' => $data['last_name'],

        'email' => $data['email'],
        'password' => Hash::make($data['password'])
      ]);
       
        return Redirect::to("login")->with('success','Great! You have Successfully Register');    
    }

        public function postLogin(Request $request)
    {
    	// dd($request);
        request()->validate([
        'email' => 'required',
        'password' => 'required',
        ]);
 
        $credentials = $request->only('email', 'password');
        if (Auth::attempt($credentials)) {
            // Authentication passed...
            return redirect()->intended('/');
        }
        return Redirect::to("login")->withSuccess('Oppes! You have entered invalid credentials');
    }


     public function logout() {
        Session::flush();
        Auth::logout();
        return Redirect('login');
    }
}
