<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class AdsImages extends Model
{
     protected $fillable = [
        'ad_id','image',
    ];
}
