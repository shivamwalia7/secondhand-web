<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Ads extends Model
{
    public function images()
    {
       return $this->hasOne(AdsImages::class,'ad_id','id');
    	
    }
     public function multipleimages()
    {
       return $this->hasMany(AdsImages::class,'ad_id','id');
        
    }

    public function category()
    {
       return $this->hasOne(Categories::class,'id','category_id');
    	
    }
     public function user()
    {
       return $this->hasOne('App\User','id','posted_by');
        
    }
}
