<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });



// Route::get('/login','HomeController@login');
// Route::get('/register','HomeController@register');
Route::get('/error','HomeController@error');

Route::get('/login', 'AuthController@index');
Route::post('post-login', 'AuthController@postLogin'); 
Route::get('/registration', 'AuthController@registration');
Route::post('post-registration', 'AuthController@postRegistration'); 


// Route::middleware('auth')->group(function () {


// }
Route::get('/ad-details/{id}','HomeController@AdDetails');
Route::post('/search','HomeController@Search');



Route::middleware(['basicauth'])->group(function () {
Route::get('logout', 'AuthController@logout');

Route::get('/myaccount','HomeController@myaccount');
Route::match(['get','post'],'/postad','HomeController@postads');
Route::get('/getsubcategories/{id}','HomeController@getSubcategoriesById');
Route::get('/deletead/{id}','HomeController@DeleteAdByUser');
Route::post('/changepassword','HomeController@ChangePassword');
Route::post('/updateprofile','HomeController@UpdateProfile');





// Route::get('/ad-details/{id}','HomeController@AdDetails');






	});


Route::get('/','HomeController@index');
Route::get('/about','HomeController@about');
Route::get('/contact','HomeController@contact');



